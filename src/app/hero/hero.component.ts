import { Component, OnInit } from '@angular/core';
import { HEROES } from '../mock-heroes';
import { Hero } from '../hero';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {

heroes = HEROES;
hero: Hero

  constructor() { }

  ngOnInit() {
  }

  onSelect(hero: Hero){
    this.hero = hero 
  }

  zerar(){
    this.hero = null
  }
}
